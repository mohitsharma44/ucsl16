These notes are accompanied with the video lectures found here:

ucsl-probabilities-stan                         https://www.youtube.com/watch?v=_in2ZrW8rkY

conditional-probabilities-bayes-stan.mp4        https://www.youtube.com/watch?v=tKYnNMqtEIs

ucsl-optimization-stan.mp4                      https://www.youtube.com/watch?v=r6ChM41fX6E

ucsl-probabilities-distribution-federica.mp4    https://www.youtube.com/watch?v=SrHK68boqwU

intro-to-linear-algebra-federica.mp4            https://www.youtube.com/watch?v=etKlgduscDg

intro-to-linear-algebra2-federica.mp4           https://www.youtube.com/watch?v=q8DYKY8gxcE   


Also note that these lectures are cloned from Dr. Federica's github repository found here:
https://github.com/fedhere/UInotebooks/tree/master/UCSL2016

Pls remember: 
- Some notebooks *might* give you some issues when you try to run it. Dont fret. The code is just meant to be used as a reference. You are not exepected to run this code.
- It is better if you open the notebooks from Dr. Federica's github link.
